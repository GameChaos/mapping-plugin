#include <sourcemod>
#include <sdktools>

#include <gamechaos>

#pragma newdecls required
#pragma semicolon 1

#define PROP_MAX_NORMAL_SCALE 64.0 // max size of prop when modelscale is 1
#define PROP_NAME "models/gamechaos/props/mappingplugin/cube.mdl"
// TODO: ladders. using a $surfaceprop ladder in the .qc doesn't work with the bounding box thing :(
// #define LADDER_PROP_NAME "models/gamechaos/props/mappingplugin/cube_ladder.mdl"
#define NULL_MODEL "models/props_doors/null.mdl"

#define EF_NODRAW (1 << 5)

#define EPSILON 0.0000001
#define MAX_BLOCK_SIZE 1024.0

#define DEFAULT_GRID_SIZE 8.0
#define MAX_GRID_SIZE 512.0

#define WORLD_MIN_SIZE -16384.0
#define WORLD_MAX_SIZE 16384.0

#define INVALID_SIZE GC_FLOAT_LARGEST_POSITIVE
#define INVALID_VECTOR view_as<float>({INVALID_SIZE, INVALID_SIZE, INVALID_SIZE})

// these are shared with redo as well.
#define UNDO_REPEAT_GRACETIME 0.5 // time to wait before starting to repeat undo.
#define UNDO_REPEAT_INTERVAL 0.4  // time between undo repeats

#define FILE_IDENTIFIER "gcmp"
#define FILE_MAGIC_NUMBER 0xD000D000
#define FILE_VERSION 0

#define FILE_EXTENSION ".gcmpl"

#define SAVED_FILE_PATH "data/mappingplugin/"

#define DEFAULT_BRUSH_SIZE view_as<float>({64.0, 64.0, 64.0})

enum SurroundingBoundsType
{
	USE_OBB_COLLISION_BOUNDS = 0,
	USE_BEST_COLLISION_BOUNDS, // Always use the best bounds (most expensive)
	USE_HITBOXES,
	USE_SPECIFIED_BOUNDS,
	USE_GAME_CODE,
	USE_ROTATION_EXPANDED_BOUNDS,
	USE_COLLISION_BOUNDS_NEVER_VPHYSICS,
	USE_ROTATION_EXPANDED_SEQUENCE_BOUNDS,

	SURROUNDING_TYPE_BIT_COUNT = 3
};

enum SolidType
{
	SOLID_NONE = 0, // no solid model
	SOLID_BSP = 1, // a BSP tree
	SOLID_BBOX = 2, // an AABB
	SOLID_OBB = 3, // an OBB (not implemented yet)
	SOLID_OBB_YAW = 4, // an OBB, constrained so that it can only yaw
	SOLID_CUSTOM = 5, // Always call into the entity for tests
	SOLID_VPHYSICS = 6, // solid vphysics object, get vcollide from the model and collide with that
	SOLID_LAST,
};

enum SolidFlags
{
	FSOLID_CUSTOMRAYTEST        = 0x0001,    // Ignore solid type + always call into the entity for ray tests
	FSOLID_CUSTOMBOXTEST        = 0x0002,    // Ignore solid type + always call into the entity for swept box tests
	FSOLID_NOT_SOLID            = 0x0004,    // Are we currently not solid?
	FSOLID_TRIGGER                = 0x0008,    // This is something may be collideable but fires touch functions
											// even when it's not collideable (when the FSOLID_NOT_SOLID flag is set)
	FSOLID_NOT_STANDABLE        = 0x0010,    // You can't stand on this
	FSOLID_VOLUME_CONTENTS        = 0x0020,    // Contains volumetric contents (like water)
	FSOLID_FORCE_WORLD_ALIGNED    = 0x0040,    // Forces the collision rep to be world-aligned even if it's SOLID_BSP or SOLID_VPHYSICS
	FSOLID_USE_TRIGGER_BOUNDS    = 0x0080,    // Uses a special trigger bounds separate from the normal OBB
	FSOLID_ROOT_PARENT_ALIGNED    = 0x0100,    // Collisions are defined in root parent's local coordinate space
	FSOLID_TRIGGER_TOUCH_DEBRIS    = 0x0200,    // This trigger will touch debris objects
	
	FSOLID_MAX_BITS    = 10
};

enum Collision_Group
{
	COLLISION_GROUP_NONE  = 0,
	COLLISION_GROUP_DEBRIS,            // Collides with nothing but world and static stuff
	COLLISION_GROUP_DEBRIS_TRIGGER, // Same as debris, but hits triggers
	COLLISION_GROUP_INTERACTIVE_DEBRIS,    // Collides with everything except other interactive debris or debris
	COLLISION_GROUP_INTERACTIVE,    // Collides with everything except interactive debris or debris
	COLLISION_GROUP_PLAYER,
	COLLISION_GROUP_BREAKABLE_GLASS,
	COLLISION_GROUP_VEHICLE,
	COLLISION_GROUP_PLAYER_MOVEMENT,  // For HL2, same as Collision_Group_Player, for
										// TF2, this filters out other players and CBaseObjects
	COLLISION_GROUP_NPC,            // Generic NPC group
	COLLISION_GROUP_IN_VEHICLE,        // for any entity inside a vehicle
	COLLISION_GROUP_WEAPON,            // for any weapons that need collision detection
	COLLISION_GROUP_VEHICLE_CLIP,    // vehicle clip brush to restrict vehicle movement
	COLLISION_GROUP_PROJECTILE,        // Projectiles!
	COLLISION_GROUP_DOOR_BLOCKER,    // Blocks entities not permitted to get near moving doors
	COLLISION_GROUP_PASSABLE_DOOR,    // Doors that the player shouldn't collide with
	COLLISION_GROUP_DISSOLVING,        // Things that are dissolving are in this group
	COLLISION_GROUP_PUSHAWAY,        // Nonsolid on client and server, pushaway in player code
	
	COLLISION_GROUP_NPC_ACTOR,        // Used so NPCs in scripts ignore the player.
	COLLISION_GROUP_NPC_SCRIPTED,    // USed for NPCs in scripts that should not collide with each other
	
	LAST_SHARED_COLLISION_GROUP
};

enum
{
	EDITSTATE_NONE  = 0,
	EDITSTATE_SCALE = 1,
	EDITSTATE_MOVE  = 2
}

enum
{
	SIDE_NONE   = -1,
	SIDE_NORTH  = 0,
	SIDE_SOUTH  = 1,
	SIDE_EAST   = 2,
	SIDE_WEST   = 3,
	SIDE_TOP    = 4,
	SIDE_BOTTOM = 5,
	SIDE_COUNT  = 6
}

enum
{
	ACTION_INVALID = -1,
	ACTION_MOVE    = 0,
	ACTION_SCALE   = 1,
	ACTION_DELETE  = 2,
	ACTION_CREATE  = 3,
}
/*
enum struct ActiveBrush
{
	int brushIndex;
	int grabbedFace;
	float initialGrabPos[3];
	float initialGrabBrushOrigin[3];
	float initialGrabBrushSize[3];
}
*/
enum struct CuboidBrush
{
	float size[3];
	float origin[3];
}

enum struct UndoEntry
{
	int undoAction; // ALWAYS first
	int brushIndex;
	float oldSize[3];
	float oldOrigin[3];
	float newSize[3];
	float newOrigin[3];
}

stock char g_szSideNames[][] = {
	"SIDE_NORTH",
	"SIDE_SOUTH",
	"SIDE_EAST",
	"SIDE_WEST",
	"SIDE_TOP",
	"SIDE_BOTTOM"
};

float g_fNormals[][3] = {
	{  1.0,  0.0,  0.0 }, // north
	{ -1.0,  0.0,  0.0 }, // south
	{  0.0,  1.0,  0.0 }, // east
	{  0.0, -1.0,  0.0 }, // west
	{  0.0,  0.0,  1.0 }, // top
	{  0.0,  0.0, -1.0 }  // bottom
};

ArrayList g_alBrushes; // array of entity references to brush props.
ArrayList g_alUndoState[MAXPLAYERS + 1];
int g_iUndoIndex[MAXPLAYERS + 1];

int g_iActiveBrushIndex[MAXPLAYERS + 1];
int g_iSelectedBrushFace[MAXPLAYERS + 1];
float g_fInitialSelectionPos[MAXPLAYERS + 1][3];
float g_fInitialSelectionBrushOrigin[MAXPLAYERS + 1][3];
float g_fInitialSelectionSize[MAXPLAYERS + 1][3];

bool g_bScalingBrush[MAXPLAYERS + 1];
bool g_bMovingBrush[MAXPLAYERS + 1];

float g_fGridSize[MAXPLAYERS + 1];

int g_iBeam;
bool g_bLateLoad;

#include "mappingplugin/commands.sp"

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	g_bLateLoad = late;
	return APLRes_Success;
}

public void OnPluginStart()
{
	// TODO: fix possible common naming conflicts?
	RegConsoleCmd("+scalebrush", Command_StartScaleBrush);
	RegConsoleCmd("-scalebrush", Command_EndScaleBrush);
	RegConsoleCmd("+movebrush", Command_StartMovebrush);
	RegConsoleCmd("-movebrush", Command_EndMovebrush);
	RegConsoleCmd("sm_createbrush", Command_SmCreatebrush);
	RegConsoleCmd("sm_duplicatebrush", Command_SmDuplicatebrush);
	RegConsoleCmd("sm_deletebrush", Command_SmDeletebrush);
	RegConsoleCmd("sm_mp_undo", Command_SmMpUndo);
	RegConsoleCmd("sm_mp_redo", Command_SmMpRedo);
	RegConsoleCmd("sm_doublegrid", Command_SmDoublegrid);
	RegConsoleCmd("sm_halvegrid", Command_SmHalvegrid);
	RegConsoleCmd("sm_gridsize", Command_SmGridsize);
	RegConsoleCmd("sm_savemap", Command_SmSavemap);
	RegConsoleCmd("sm_loadmap", Command_SmLoadmap);
	RegConsoleCmd("sm_exportvmf", Command_SmExportvmf);
	
	if (g_bLateLoad)
	{
		for (int client = 1; client <= MaxClients; client++)
		{
			if (GCIsValidClient(client))
			{
				OnClientPostAdminCheck(client);
			}
		}
	}
}

public void OnPluginEnd()
{
	// clean up after ourselves.
	for (int i = 0; i < g_alBrushes.Length; i++)
	{
		KillEntity(g_alBrushes.Get(i));
	}
}

public void OnMapStart()
{
	if (g_alBrushes == null)
	{
		g_alBrushes = new ArrayList();
	}
	else
	{
		g_alBrushes.Clear();
	}
	
	// TODO: ?
	AddFileToDownloadsTable(PROP_NAME);
	AddFileToDownloadsTable("models/gamechaos/props/mappingplugin/cube.vvd");
	AddFileToDownloadsTable("models/gamechaos/props/mappingplugin/cube.dx90.vtx");
	AddFileToDownloadsTable("materials/models/gamechaos/props/mappingplugin/top.vmt");
	AddFileToDownloadsTable("materials/models/gamechaos/props/mappingplugin/side.vmt");
	AddFileToDownloadsTable("materials/models/gamechaos/props/mappingplugin/bottom.vmt");
	
	g_iBeam = PrecacheModel("materials/sprites/laser.vmt", true);
	
	PrecacheModel(PROP_NAME);
	PrecacheModel(NULL_MODEL);
}

public void OnClientPostAdminCheck(int client)
{
	ResetClientVariables(client);
}

void ResetClientVariables(int client)
{
	g_iActiveBrushIndex[client] = -1;
	g_iSelectedBrushFace[client] = -1;
	g_fInitialSelectionPos[client] = NULL_VECTOR;
	g_fInitialSelectionBrushOrigin[client] = NULL_VECTOR;
	g_fInitialSelectionSize[client] = NULL_VECTOR;
	g_fGridSize[client] = DEFAULT_GRID_SIZE;
	g_bScalingBrush[client] = false;
	g_bMovingBrush[client] = false;
	
	if (g_alUndoState[client] == null)
	{
		g_alUndoState[client] = new ArrayList(sizeof UndoEntry);
	}
	else
	{
		g_alUndoState[client].Clear();
	}
	g_iUndoIndex[client] = 0;
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if (!GCIsValidClient(client))
	{
		return Plugin_Continue;
	}
	
	float origin[3];
	float size[3];
	if (g_iActiveBrushIndex[client] != -1)
	{
		int selectedBrush = g_alBrushes.Get(g_iActiveBrushIndex[client]);
		
		if (!IsValidEntity(selectedBrush))
		{
			g_iActiveBrushIndex[client] = -1;
			return Plugin_Continue;
		}
		
		GetEntPropVector(selectedBrush, Prop_Send, "m_vecOrigin", origin);
		GetEntPropVector(selectedBrush, Prop_Send, "m_vecMaxs", size);
		
		if (g_bMovingBrush[client] || g_bScalingBrush[client])
		{
			float prevSize[3];
			float prevOrigin[3];
			prevSize = size;
			prevOrigin = origin;
			
			// set stuff up for LineLineInterSection
			float eyePosStart[3];
			GetClientEyePosition(client, eyePosStart);
			
			float eyeVector[3];
			GetAngleVectors(angles, eyeVector, NULL_VECTOR, NULL_VECTOR);
			
			float eyePosEnd[3];
			eyePosEnd = eyeVector;
			ScaleVector(eyePosEnd, MAX_BLOCK_SIZE);
			AddVectors(eyePosEnd, eyePosStart, eyePosEnd);
			
			float selectionPosStart[3];
			selectionPosStart = g_fNormals[g_iSelectedBrushFace[client]];
			ScaleVector(selectionPosStart, -MAX_BLOCK_SIZE);
			AddVectors(selectionPosStart, g_fInitialSelectionPos[client], selectionPosStart);
			
			float selectionPosEnd[3];
			selectionPosEnd = g_fNormals[g_iSelectedBrushFace[client]];
			ScaleVector(selectionPosEnd, MAX_BLOCK_SIZE);
			AddVectors(selectionPosEnd, g_fInitialSelectionPos[client], selectionPosEnd);
			
			float intersectPoint1[3];
			float intersectPoint2[3];
			
			float fracA, fracB;
			
			LineLineIntersection(
				eyePosStart,
				eyePosEnd,
				selectionPosStart,
				selectionPosEnd,
				intersectPoint1, intersectPoint2,
				fracA, fracB
			);
			
			float distanceVec[3];
			SubtractVectors(intersectPoint1, g_fInitialSelectionPos[client], distanceVec);
			
			float distance = GetVectorDotProduct(distanceVec, g_fNormals[g_iSelectedBrushFace[client]]);
			
			bool negativeSide = g_iSelectedBrushFace[client] == SIDE_SOUTH
				|| g_iSelectedBrushFace[client] == SIDE_WEST
				|| g_iSelectedBrushFace[client] == SIDE_BOTTOM;
			
			int selectedAxis = g_iSelectedBrushFace[client] / 2;
			
			if (g_bScalingBrush[client])
			{
				if (negativeSide)
				{
					origin = g_fInitialSelectionBrushOrigin[client];
				}
				
				size = g_fInitialSelectionSize[client];
				float addDist = distance * FloatAbs(g_fNormals[g_iSelectedBrushFace[client]][selectedAxis]);
				
				// clamp size between 1.0 and MAX_BLOCK_SIZE
				addDist = GCFloatMin(addDist, MAX_BLOCK_SIZE - size[selectedAxis]);
				addDist = GCFloatMax(addDist, g_fGridSize[client] - size[selectedAxis]);
				
				size[selectedAxis] += addDist;
				if (negativeSide)
				{
					origin[selectedAxis] -= addDist;
					
					float newOrigin = float(RoundFloat(origin[selectedAxis] / g_fGridSize[client])) * g_fGridSize[client];
					size[selectedAxis] -= newOrigin - origin[selectedAxis];
					origin[selectedAxis] = newOrigin;
				}
				else
				{
					size[selectedAxis] = float(RoundFloat((size[selectedAxis] + origin[selectedAxis]) / g_fGridSize[client])) * g_fGridSize[client] - origin[selectedAxis];
				}
				
				// cap size
				for (int i = 0; i < 3; i++)
				{
					size[i] = GCFloatMin(MAX_BLOCK_SIZE, size[i]);
				}
				
				// only recreate the brush if it was moved or changed in any way.
				if (!GCVectorsEqual(origin, prevOrigin) || !GCVectorsEqual(size, prevSize))
				{
					int newBrush = CreateCuboidProp(origin, size);
					
					if (!IsValidEntity(selectedBrush))
					{
						PrintToChatAll("Couldn't scale the prop, sorry bro!");
					}
					
					KillEntity(selectedBrush);
					selectedBrush = newBrush;
					
					g_alBrushes.Set(g_iActiveBrushIndex[client], selectedBrush);
				}
			}
			
			if (g_bMovingBrush[client])
			{
				origin = g_fInitialSelectionBrushOrigin[client];
				
				float addDist = distance * g_fNormals[g_iSelectedBrushFace[client]][selectedAxis];
				origin[selectedAxis] += addDist;
				origin[selectedAxis] = GCFloatClamp(origin[selectedAxis], WORLD_MIN_SIZE, WORLD_MAX_SIZE);
				
				if (negativeSide)
				{
					origin[selectedAxis] = float(RoundFloat(origin[selectedAxis] / g_fGridSize[client])) * g_fGridSize[client];
				}
				else
				{
					origin[selectedAxis] += size[selectedAxis];
					origin[selectedAxis] = float(RoundFloat(origin[selectedAxis] / g_fGridSize[client])) * g_fGridSize[client];
					origin[selectedAxis] -= size[selectedAxis];
				}
				
				// AlignVectorToGrid(origin, g_fGridSize[client]);
				TeleportEntity(selectedBrush, origin, NULL_VECTOR, NULL_VECTOR);
				
				// PrintToChat(client, "%x %x %x  %x %x %x", origin[0], origin[1], origin[2], prevOrigin[0], prevOrigin[1], prevOrigin[2]);
			}
		}
		
		// draw bounding box of selected brush
		float maxs[3];
		GetEntPropVector(selectedBrush, Prop_Send, "m_vecMaxs", maxs);
		
		if (GetGameTickCount() % 12 == 0)
		{
			// white box for me!
			GCTE_SendBeamBox(client, origin, view_as<float>({0, 0, 0}), size, g_iBeam, 0, 0.1, 0.5, .EndWidth = 0.5);
			// red box for everyone else!
			for (int otherClient = 1; otherClient <= MaxClients; otherClient++)
			{
				if (!GCIsValidClient(otherClient) || otherClient == client)
				{
					continue;
				}
				
				GCTE_SendBeamBox(otherClient, origin, view_as<float>({0, 0, 0}), size, g_iBeam, 0, 0.1, 0.5, .Colour = {255, 0, 0, 255}, .EndWidth = 0.5);
			}
		}
	}
	
	char hudText[256];
	SetHudTextParams(0.15, 0.2, 0.1, 255, 255, 255, 255, 0, 0.0, 0.0, 0.0);
	char szGridSize[32];
	
	FloatToString(g_fGridSize[client], szGridSize, sizeof szGridSize);
	
	GCRemoveTrailing0s(szGridSize);
	
	Format(hudText, sizeof hudText, "Grid size: %s",
		szGridSize
	);
	
	if (g_iActiveBrushIndex[client] != -1)
	{
		char szSizeX[32];
		char szSizeY[32];
		char szSizeZ[32];
		FloatToString(size[0], szSizeX, sizeof szSizeX);
		FloatToString(size[1], szSizeY, sizeof szSizeY);
		FloatToString(size[2], szSizeZ, sizeof szSizeZ);
		GCRemoveTrailing0s(szSizeX);
		GCRemoveTrailing0s(szSizeY);
		GCRemoveTrailing0s(szSizeZ);
		
		Format(hudText, sizeof hudText, "%s\nBrush size: %s %s %s", hudText, szSizeX, szSizeY, szSizeZ);
	}
	ShowHudText(client, -1, hudText);
	
	return Plugin_Continue;
}

bool TryGrabbingBrush(int client)
{
	bool result = false;
	
	g_iActiveBrushIndex[client] = -1;
	g_fInitialSelectionBrushOrigin[client] = NULL_VECTOR;
	g_fInitialSelectionSize[client] = NULL_VECTOR;
	g_iSelectedBrushFace[client] = SIDE_NONE;
	
	if (GCGetEyeRayPosition(client, NULL_VECTOR, GCTraceEntityFilterPlayer))
	{
		int entRef = EntIndexToEntRef(TR_GetEntityIndex());
		g_iActiveBrushIndex[client] = GetBrushIndex(g_alBrushes, entRef);
		
		if (g_iActiveBrushIndex[client] != -1)
		{
			// check if someone else is already editing the brush
			bool someoneElseUsing = false;
			for (int otherClient = 1; otherClient <= MaxClients; otherClient++)
			{
				if (!GCIsValidClient(otherClient) || client == otherClient)
				{
					continue;
				}
				
				if (g_iActiveBrushIndex[client] == g_iActiveBrushIndex[otherClient])
				{
					PrintToChat(client, "Can't grab a brush while someone else is editing it!");
					someoneElseUsing = true;
					result = false;
					break;
				}
			}
			
			if (!someoneElseUsing)
			{
				float origin[3];
				GetEntPropVector(entRef, Prop_Send, "m_vecOrigin", origin);
				float size[3];
				GetEntPropVector(entRef, Prop_Send, "m_vecMaxs", size);
				
				// get grabbed face
				float normal[3];
				TR_GetPlaneNormal(INVALID_HANDLE, normal);
				
				for (int i = 0; i < sizeof g_fNormals; i++)
				{
					float dot = GetVectorDotProduct(normal, g_fNormals[i]);
					
					if (dot > 0.999)
					{
						TR_GetEndPosition(g_fInitialSelectionPos[client]);
						g_fInitialSelectionBrushOrigin[client] = origin;
						g_fInitialSelectionSize[client] = size;
						g_iSelectedBrushFace[client] = i;
						// PrintToChatAll("Normal %f %f %f dot %f side %i %s", normal[0], normal[1], normal[2], dot, i, g_szSideNames[i]);
						
						result = true;
						break;
					}
				}
			}
		}
	}
	
	if (!result)
	{
		g_iActiveBrushIndex[client] = -1;
	}
	return result;
}

UndoEntry[] CreateUndoEntry(int action, int brushIndex, float oldOrigin[3], float oldSize[3], float newOrigin[3], float newSize[3])
{
	UndoEntry entry;
	entry.undoAction = action;
	entry.brushIndex = brushIndex;
	entry.oldOrigin = oldOrigin;
	entry.oldSize = oldSize;
	entry.newOrigin = newOrigin;
	entry.newSize = newSize;
	return entry;
}

void PushUndoEntry(int client, UndoEntry entry)
{
	PrintToConsole(client, "%i", g_alUndoState[client].Length);
	if (g_iUndoIndex[client] < g_alUndoState[client].Length - 1)
	{
		g_alUndoState[client].Resize(g_iUndoIndex[client] + 1);
	}
	
	g_iUndoIndex[client] = g_alUndoState[client].PushArray(entry, sizeof entry);
	
	// NOTE: DEBUG
	/*
	char text[8192];
	for (int i = 0; i < g_alUndoState[client].Length; i++)
	{
		UndoEntry entry2;
		g_alUndoState[client].GetArray(i, entry2, sizeof entry2);
		
		Format(text, sizeof text, "%s%i | %i | %.0f %.0f %.0f | %.0f %.0f %.0f | %.0f %.0f %.0f | %.0f %.0f %.0f\n",
			text,
			entry2.undoAction,
			entry2.brushIndex,
			entry2.oldOrigin[0] == INVALID_SIZE ? -1.0 : entry2.oldOrigin[0],
			entry2.oldOrigin[1] == INVALID_SIZE ? -1.0 : entry2.oldOrigin[1],
			entry2.oldOrigin[2] == INVALID_SIZE ? -1.0 : entry2.oldOrigin[2],
			entry2.oldSize[0] == INVALID_SIZE ? -1.0 : entry2.oldSize[0],
			entry2.oldSize[1] == INVALID_SIZE ? -1.0 : entry2.oldSize[1],
			entry2.oldSize[2] == INVALID_SIZE ? -1.0 : entry2.oldSize[2],
			entry2.newOrigin[0] == INVALID_SIZE ? -1.0 : entry2.newOrigin[0],
			entry2.newOrigin[1] == INVALID_SIZE ? -1.0 : entry2.newOrigin[1],
			entry2.newOrigin[2] == INVALID_SIZE ? -1.0 : entry2.newOrigin[2],
			entry2.newSize[0] == INVALID_SIZE ? -1.0 : entry2.newSize[0],
			entry2.newSize[1] == INVALID_SIZE ? -1.0 : entry2.newSize[1],
			entry2.newSize[2] == INVALID_SIZE ? -1.0 : entry2.newSize[2]
		);
	}
	PrintToConsole(client, "undoindex %i", g_iUndoIndex[client]);
	PrintToConsole(client, text);
	//*/
}

int CreateCuboidProp(const float position[3], const float size[3])
{
	int entity = CreateEntityByName("prop_dynamic_override");
	
	if (!IsValidEntity(entity))
	{
		// print error
		return INVALID_ENT_REFERENCE;
	}
	
	DispatchKeyValue(entity, "model", PROP_NAME);
	DispatchKeyValue(entity, "solid", "2"); // TODO: what is this?
	DispatchKeyValue(entity, "solidbsp", "1"); // use solid bsp
	
	// TODO: handle this better mmkay, say error to the user
	if (!DispatchSpawn(entity))
	{
		// print error
		return INVALID_ENT_REFERENCE;
	}
	
	float largestSideLength = 0.0;
	for (int i = 0; i < 3; i++)
	{
		if (size[i] > largestSideLength)
		{
			largestSideLength = size[i];
		}
	}
	
	// largestSideLength /= ;
	
	// change the model size
	char szLargestSideLength[32];
	FloatToString(largestSideLength / PROP_MAX_NORMAL_SCALE, szLargestSideLength, sizeof szLargestSideLength);
	DispatchKeyValue(entity, "modelscale", szLargestSideLength);
	
	SetEntPropVector(entity, Prop_Send, "m_vecMins", view_as<float>({0.0, 0.0, 0.0}));
	SetEntPropVector(entity, Prop_Send, "m_vecSpecifiedSurroundingMins", view_as<float>({0.0, 0.0, 0.0}));
	SetEntPropVector(entity, Prop_Send, "m_vecMaxs", size);
	SetEntPropVector(entity, Prop_Send, "m_vecSpecifiedSurroundingMaxs", size);
	SetEntProp(entity, Prop_Send, "m_nSolidType", SOLID_BBOX);
	
	// rotate it 90 degrees. it's off by 90 for some reason. TODO: fix?
	TeleportEntity(entity, position, view_as<float>({0.0, -90.0, 0.0}), NULL_VECTOR);
	
	/*
	// this model ("models/gamechaos/props/mappingplugin/cube.mdl") has 12 sequences:
	// if all the parameters are set to 1.0 and modelscale is 1.0, then the model is a 1x1x1 cube.
	{
		// corner x,0,0
		// 0 - controls x component of the corner
		
		// corner 0,y,0
		// 1 - controls y component of the corner
		
		// corner 0,0,z
		// 2 - controls z component of the corner
		
		// corner x,y,0
		// 3 - controls x component of the corner
		// 4 - controls y component of the corner
		
		// corner x,0,z
		// 5 - controls x component of the corner
		// 6 - controls z component of the corner
		
		// corner 0,y,z
		// 7 - controls y component of the corner
		// 8 - controls z component of the corner
		
		// corner x,y,z
		// 9  - controls x component of the corner
		// 10 - controls y component of the corner
		// 11 - controls z component of the corner
	}
	*/
	
	// resize it proportionally to fit in 1x1x1 cube.
	float normalisedSize[3];
	normalisedSize = size;
	if (largestSideLength != 0.0)
	{
		normalisedSize[0] /= largestSideLength;
		normalisedSize[1] /= largestSideLength;
		normalisedSize[2] /= largestSideLength;
	}
	// TODO: use an array instead? might be faster, who knows.
	SetPoseParameter(entity, normalisedSize[0], 0);
	SetPoseParameter(entity, normalisedSize[1], 1);
	SetPoseParameter(entity, normalisedSize[2], 2);
	
	SetPoseParameter(entity, normalisedSize[0], 3);
	SetPoseParameter(entity, normalisedSize[1], 4);
	SetPoseParameter(entity, normalisedSize[0], 5);
	
	SetPoseParameter(entity, normalisedSize[2], 6);
	SetPoseParameter(entity, normalisedSize[1], 7);
	SetPoseParameter(entity, normalisedSize[2], 8);
	
	SetPoseParameter(entity, normalisedSize[0], 9);
	SetPoseParameter(entity, normalisedSize[1], 10);
	SetPoseParameter(entity, normalisedSize[2], 11);
	
	return EntIndexToEntRef(entity);
}

int GetBrushIndex(ArrayList array, int entRef)
{
	int result = -1;
	for (int i = 0; i < array.Length; i++)
	{
		int value = array.Get(i);
		if (value != INVALID_ENT_REFERENCE
			&& value == entRef)
		{
			result = i;
			break;
		}
	}
	
	return result;
}

// -----------------
// utility functions
// -----------------

stock void SetPoseParameter(int entity, float value, int index)
{
	// TODO: bounds checking and such?
	SetEntPropFloat(entity, Prop_Send, "m_flPoseParameter", value, index);
}

stock void KillEntity(int entity)
{
	if (IsValidEntity(entity) && entity != 0)
	{
		AcceptEntityInput(entity, "kill");
	}
}

stock void AlignSizeToGrid(float size[3], float gridSize)
{
	for (int i = 0; i < 3; i++)
	{
		size[i] = float(RoundFloat(size[i] / gridSize)) * gridSize;
		if (size[i] < gridSize)
		{
			size[i] = gridSize;
		}
	}
}

stock void AlignVectorToGrid(float size[3], float gridSize)
{
	for (int i = 0; i < 3; i++)
	{
		size[i] = float(RoundFloat(size[i] / gridSize)) * gridSize;
	}
}

// only use for vectors that can be bounded in -16384 to 16384.
stock bool IsInvalidVector(float vector[3])
{
	return vector[0] == INVALID_SIZE && vector[1] == INVALID_SIZE && vector[2] == INVALID_SIZE;
}

/*
 http://paulbourke.net/geometry/pointlineplane/lineline.c
 Calculate the line segment PaPb that is the shortest route between
 two lines P1P2 and P3P4. Calculate also the values of mua and mub where
    Pa = P1 + mua (P2 - P1)
    Pb = P3 + mub (P4 - P3)
 Return false if no solution exists.
*/
stock bool LineLineIntersection(
	const float p1[3],
	const float p2[3],
	const float p3[3],
	const float p4[3],
	float pa[3], float pb[3],
	float &mua, float &mub)
{
	float p13[3], p43[3], p21[3];
	
	p13[0] = p1[0] - p3[0];
	p13[1] = p1[1] - p3[1];
	p13[2] = p1[2] - p3[2];
	p43[0] = p4[0] - p3[0];
	p43[1] = p4[1] - p3[1];
	p43[2] = p4[2] - p3[2];
	
	if (FloatAbs(p43[0]) < EPSILON && FloatAbs(p43[1]) < EPSILON && FloatAbs(p43[2]) < EPSILON)
	{
		return false;
	}
	
	p21[0] = p2[0] - p1[0];
	p21[1] = p2[1] - p1[1];
	p21[2] = p2[2] - p1[2];
	if (FloatAbs(p21[0]) < EPSILON && FloatAbs(p21[1]) < EPSILON && FloatAbs(p21[2]) < EPSILON)
	{
		return false;
	}
	
	float d1343, d4321, d1321, d4343, d2121;
	d1343 = p13[0] * p43[0] + p13[1] * p43[1] + p13[2] * p43[2];
	d4321 = p43[0] * p21[0] + p43[1] * p21[1] + p43[2] * p21[2];
	d1321 = p13[0] * p21[0] + p13[1] * p21[1] + p13[2] * p21[2];
	d4343 = p43[0] * p43[0] + p43[1] * p43[1] + p43[2] * p43[2];
	d2121 = p21[0] * p21[0] + p21[1] * p21[1] + p21[2] * p21[2];
	
	float numer, denom;
	denom = d2121 * d4343 - d4321 * d4321;
	
	if (FloatAbs(denom) < EPSILON)
	{
		return false;
	}
	
	numer = d1343 * d4321 - d1321 * d4343;
	
	mua = numer / denom;
	mub = (d1343 + d4321 * (mua)) / d4343;
	
	pa[0] = p1[0] + mua * p21[0];
	pa[1] = p1[1] + mua * p21[1];
	pa[2] = p1[2] + mua * p21[2];
	pb[0] = p3[0] + mub * p43[0];
	pb[1] = p3[1] + mub * p43[1];
	pb[2] = p3[2] + mub * p43[2];
	
	return true;
}
