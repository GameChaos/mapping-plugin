
#define TOP_MATERIAL "concrete/hr_c/hr_concrete_polished_001"
#define SIDE_MATERIAL "de_mirage/hr_mirage/mirage_plaster_1"
#define BOTTOM_MATERIAL "concrete/ceiling03"

static char g_szVmfStart[] = "versioninfo\n"...
	"{\n"...
	"	\"editorversion\" \"400\"\n"...
	"	\"editorbuild\" \"8456\"\n"...
	"	\"mapversion\" \"1\"\n"...
	"	\"formatversion\" \"100\"\n"...
	"	\"prefab\" \"0\"\n"...
	"}\n"...
	"visgroups\n"...
	"{\n"...
	"}\n"...
	"viewsettings\n"...
	"{\n"...
	"	\"bSnapToGrid\" \"1\"\n"...
	"	\"bShowGrid\" \"1\"\n"...
	"	\"bShowLogicalGrid\" \"0\"\n"...
	"	\"nGridSpacing\" \"4\"\n"...
	"	\"bShow3DGrid\" \"0\"\n"...
	"}\n"...
	"world\n"...
	"{\n"...
	"	\"id\" \"1\"\n"...
	"	\"mapversion\" \"1\"\n"...
	"	\"classname\" \"worldspawn\"\n"...
	"	\"detailmaterial\" \"detail/detailsprites\"\n"...
	"	\"detailvbsp\" \"detail.vbsp\"\n"...
	"	\"maxpropscreenwidth\" \"-1\"\n"...
	"	\"skyname\" \"sky_dust\"\n";

static char g_szVmfEnd[] = "}\n"...
	"cameras\n"...
	"{\n"...
	"	\"activecamera\" \"-1\"\n"...
	"}\n"...
	"cordons\n"...
	"{\n"...
	"	\"active\" \"0\"\n"...
	"}\n";

// VMF plane values
static float g_fPlanes[SIDE_COUNT][3][3] = {
	{{ 1.0, 1.0, 0.0 }, { 1.0, 0.0, 0.0 }, { 1.0, 0.0, 1.0 }}, // SIDE_NORTH
	{{ 0.0, 1.0, 1.0 }, { 0.0, 0.0, 1.0 }, { 0.0, 0.0, 0.0 }}, // SIDE_SOUTH
	{{ 1.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 1.0 }}, // SIDE_EAST
	{{ 1.0, 1.0, 1.0 }, { 0.0, 1.0, 1.0 }, { 0.0, 1.0, 0.0 }}, // SIDE_WEST
	{{ 0.0, 1.0, 1.0 }, { 1.0, 1.0, 1.0 }, { 1.0, 0.0, 1.0 }}, // SIDE_TOP
	{{ 0.0, 0.0, 0.0 }, { 1.0, 0.0, 0.0 }, { 1.0, 1.0, 0.0 }}, // SIDE_BOTTOM
};

public Action Command_StartScaleBrush(int client, int args)
{
	if (g_bMovingBrush[client])
	{
		PrintToChat(client, "Can't scale brush while moving the brush");
		return Plugin_Handled;
	}
	
	g_bScalingBrush[client] = true;
	TryGrabbingBrush(client);
	
	return Plugin_Handled;
}

public Action Command_EndScaleBrush(int client, int args)
{
	if (!g_bScalingBrush[client])
	{
		return Plugin_Handled;
	}
	g_bScalingBrush[client] = false;
	
	if (g_iActiveBrushIndex[client] == -1)
	{
		return Plugin_Handled;
	}
	
	// TODO: more error checking?
	int entRef = g_alBrushes.Get(g_iActiveBrushIndex[client]);
	if (IsValidEntity(entRef))
	{
		float origin[3];
		GetEntPropVector(entRef, Prop_Send, "m_vecOrigin", origin);
		float size[3];
		GetEntPropVector(entRef, Prop_Send, "m_vecMaxs", size);
		
		if (!GCVectorsEqual(origin, g_fInitialSelectionBrushOrigin[client])
			|| !GCVectorsEqual(size, g_fInitialSelectionSize[client]))
		{
			UndoEntry entry;
			entry = CreateUndoEntry(
				ACTION_SCALE,
				g_iActiveBrushIndex[client],
				g_fInitialSelectionBrushOrigin[client],
				g_fInitialSelectionSize[client],
				origin,
				size
			);
			PushUndoEntry(client, entry);
		}
	}
	else
	{
		PrintToChat(client, "massive error wtf!!!");
	}
	
	return Plugin_Handled;
}

public Action Command_StartMovebrush(int client, int args)
{
	if (g_bScalingBrush[client])
	{
		PrintToChat(client, "Can't move brush while scaling the brush");
		return Plugin_Handled;
	}
	
	g_bMovingBrush[client] = true;
	TryGrabbingBrush(client);
	return Plugin_Handled;
}

public Action Command_EndMovebrush(int client, int args)
{
	if (!g_bMovingBrush[client])
	{
		return Plugin_Handled;
	}
	g_bMovingBrush[client] = false;
	
	if (g_iActiveBrushIndex[client] == -1)
	{
		return Plugin_Handled;
	}
	
	// TODO: more error checking?
	int entRef = g_alBrushes.Get(g_iActiveBrushIndex[client]);
	if (IsValidEntity(entRef))
	{
		float origin[3];
		GetEntPropVector(entRef, Prop_Data, "m_vecAbsOrigin", origin);
		float size[3];
		GetEntPropVector(entRef, Prop_Send, "m_vecMaxs", size);
		
		if (!GCVectorsEqual(origin, g_fInitialSelectionBrushOrigin[client]))
		{
			UndoEntry entry;
			entry = CreateUndoEntry(
				ACTION_SCALE,
				g_iActiveBrushIndex[client],
				g_fInitialSelectionBrushOrigin[client],
				g_fInitialSelectionSize[client],
				origin,
				size
			);
			PushUndoEntry(client, entry);
		}
	}
	else
	{
		PrintToChat(client, "massive error wtf!!!");
	}
	
	return Plugin_Handled;
}

public Action Command_SmCreatebrush(int client, int args)
{
	if (!GCIsValidClient(client))
	{
		return Plugin_Handled;
	}
	
	if (g_bScalingBrush[client])
	{
		PrintToChat(client, "Can't create a brush while scaling another brush");
		return Plugin_Handled;
	}
	
	if (g_bMovingBrush[client])
	{
		PrintToChat(client, "Can't create a brush while moving another brush");
		return Plugin_Handled;
	}
	
	float result[3];
	if (GCGetEyeRayPosition(client, result, GCTraceEntityFilterPlayer))
	{
		// compensate for the origin being in the corner	
		float size[3];
		size = DEFAULT_BRUSH_SIZE;
		AlignSizeToGrid(size, g_fGridSize[client]);
		result[0] -= size[0] * 0.5;
		result[1] -= size[1] * 0.5;
		AlignVectorToGrid(result, g_fGridSize[client]);
		// TODO: error checking
		int entRef = CreateCuboidProp(result, size);
		if (IsValidEntity(entRef))
		{
			g_iActiveBrushIndex[client] = g_alBrushes.Push(entRef);
			UndoEntry entry;
			entry = CreateUndoEntry(ACTION_CREATE, g_iActiveBrushIndex[client], INVALID_VECTOR, INVALID_VECTOR, result, size);
			PushUndoEntry(client, entry);
		}
		else
		{
			PrintToChat(client, "Couldn't create a brush for whatever reason");
		}
	}
	
	return Plugin_Handled;
}

public Action Command_SmDuplicatebrush(int client, int args)
{
	if (!GCIsValidClient(client))
	{
		return Plugin_Handled;
	}
	
	if (g_bScalingBrush[client])
	{
		PrintToChat(client, "Can't create a brush while scaling another brush");
		return Plugin_Handled;
	}
	
	if (g_bMovingBrush[client])
	{
		PrintToChat(client, "Can't create a brush while moving another brush");
		return Plugin_Handled;
	}
	
	if (g_iActiveBrushIndex[client] == -1)
	{
		return Plugin_Handled;
	}
	
	int entRef = g_alBrushes.Get(g_iActiveBrushIndex[client]);
	if (IsValidEntity(entRef))
	{
		float origin[3];
		GetEntPropVector(entRef, Prop_Send, "m_vecOrigin", origin);
		float size[3];
		GetEntPropVector(entRef, Prop_Send, "m_vecMaxs", size);
		// move new brush up a bit
		origin[2] += GCFloatMax(8.0, g_fGridSize[client]);
		
		// TODO: error checking
		int newEnt = CreateCuboidProp(origin, size);
		if (IsValidEntity(newEnt))
		{
			g_iActiveBrushIndex[client] = g_alBrushes.Push(newEnt);
			UndoEntry entry;
			entry = CreateUndoEntry(ACTION_CREATE, g_iActiveBrushIndex[client], INVALID_VECTOR, INVALID_VECTOR, origin, size);
			PushUndoEntry(client, entry);
		}
		else
		{
			PrintToChat(client, "Couldn't create a brush for whatever reason");
		}
	}
	
	return Plugin_Handled;
}

public Action Command_SmDeletebrush(int client, int args)
{
	if (!GCIsValidClient(client))
	{
		return Plugin_Handled;
	}
	
	if (g_bScalingBrush[client])
	{
		PrintToChat(client, "Can't delete a brush while scaling another brush");
		return Plugin_Handled;
	}
	
	if (g_bMovingBrush[client])
	{
		PrintToChat(client, "Can't delete a brush while moving another brush");
		return Plugin_Handled;
	}
	
	if (g_iActiveBrushIndex[client] == -1)
	{
		return Plugin_Handled;
	}
	
	int entRef = g_alBrushes.Get(g_iActiveBrushIndex[client]);
	if (IsValidEntity(entRef))
	{
		float origin[3];
		GetEntPropVector(entRef, Prop_Send, "m_vecOrigin", origin);
		float size[3];
		GetEntPropVector(entRef, Prop_Send, "m_vecMaxs", size);
		
		UndoEntry entry;
		entry = CreateUndoEntry(ACTION_DELETE, g_iActiveBrushIndex[client], origin, size, INVALID_VECTOR, INVALID_VECTOR);
		PushUndoEntry(client, entry);
		
		KillEntity(entRef);
		g_alBrushes.Set(g_iActiveBrushIndex[client], INVALID_ENT_REFERENCE);
	}
	else
	{
		PrintToChat(client, "Couldn't delete brush");
	}
	
	g_iActiveBrushIndex[client] = -1;
	
	return Plugin_Handled;
}

public Action Command_SmMpUndo(int client, int args)
{
	// TODO: fix bug with race condition or whatever happening when there's 2 players
	// and one player edits a block, then the other player deletes the block, but then
	// the first player tries to undo.
	if (g_bScalingBrush[client])
	{
		PrintToChat(client, "Can't undo while scaling another brush");
		return Plugin_Handled;
	}
	
	if (g_bMovingBrush[client])
	{
		PrintToChat(client, "Can't undo while moving another brush");
		return Plugin_Handled;
	}
	
	if (g_iUndoIndex[client] >= 0)
	{
		UndoEntry entry;
		g_alUndoState[client].GetArray(g_iUndoIndex[client], entry, sizeof entry);
		
		int entRef = g_alBrushes.Get(entry.brushIndex);
		bool success = false;
		
		switch (entry.undoAction)
		{
			case ACTION_MOVE:
			{
				if (IsValidEntity(entRef) && !IsInvalidVector(entry.oldOrigin))
				{
					TeleportEntity(entRef, entry.oldOrigin, NULL_VECTOR, NULL_VECTOR);
					g_iActiveBrushIndex[client] = entry.brushIndex;
					success = true;
				}
			}
			case ACTION_SCALE:
			{
				if (IsValidEntity(entRef) && !IsInvalidVector(entry.oldOrigin)
					&& !IsInvalidVector(entry.oldSize))
				{
					int newEnt = CreateCuboidProp(entry.oldOrigin, entry.oldSize);
					if (IsValidEntity(newEnt))
					{
						KillEntity(entRef);
						entRef = newEnt;
						g_alBrushes.Set(entry.brushIndex, newEnt);
						g_iActiveBrushIndex[client] = entry.brushIndex;
						
						success = true;
					}
				}
			}
			case ACTION_CREATE:
			{
				if (IsValidEntity(entRef))
				{
					KillEntity(entRef);
					entRef = INVALID_ENT_REFERENCE;
					g_alBrushes.Set(entry.brushIndex, INVALID_ENT_REFERENCE);
					
					success = true;
				}
			}
			case ACTION_DELETE:
			{
				entRef = CreateCuboidProp(entry.oldOrigin, entry.oldSize);
				if (IsValidEntity(entRef))
				{
					g_alBrushes.Set(entry.brushIndex, entRef);
					g_iActiveBrushIndex[client] = entry.brushIndex;
					success = true;
				}
			}
		}
		
		if (success)
		{
			g_iUndoIndex[client]--;
		}
		else
		{
			// FUCKING PANIC
			PrintToChat(client, "WHAT THE FUCK WHAT HAPEHAPESHRASPEIJRHSAOLKEJR");
		}
	}
	else
	{
		PrintToChat(client, "Couldn't undo anything");
	}
	return Plugin_Handled;
}

public Action Command_SmMpRedo(int client, int args)
{
	if (g_bScalingBrush[client])
	{
		PrintToChat(client, "Can't redo while scaling another brush");
		return Plugin_Handled;
	}
	
	if (g_bMovingBrush[client])
	{
		PrintToChat(client, "Can't redo while moving another brush");
		return Plugin_Handled;
	}
	
	if (g_iUndoIndex[client] < g_alUndoState[client].Length - 1)
	{
		// do stuff
		g_iUndoIndex[client]++;
		
		
		UndoEntry entry;
		g_alUndoState[client].GetArray(g_iUndoIndex[client], entry, sizeof entry);
		
		int entRef = g_alBrushes.Get(entry.brushIndex);
		bool success = false;
		
		switch (entry.undoAction)
		{
			case ACTION_MOVE:
			{
				if (IsValidEntity(entRef) && !IsInvalidVector(entry.newOrigin))
				{
					TeleportEntity(entRef, entry.newOrigin, NULL_VECTOR, NULL_VECTOR);
					success = true;
				}
			}
			case ACTION_SCALE:
			{
				if (IsValidEntity(entRef) && !IsInvalidVector(entry.newOrigin)
					&& !IsInvalidVector(entry.newSize))
				{
					int newEnt = CreateCuboidProp(entry.newOrigin, entry.newSize);
					if (IsValidEntity(newEnt))
					{
						KillEntity(entRef);
						g_alBrushes.Set(entry.brushIndex, newEnt);
						entRef = newEnt;
						
						success = true;
					}
				}
			}
			case ACTION_CREATE:
			{
				entRef = CreateCuboidProp(entry.newOrigin, entry.newSize);
				if (IsValidEntity(entRef))
				{
					g_alBrushes.Set(entry.brushIndex, entRef);
					g_iActiveBrushIndex[client] = entry.brushIndex;
					success = true;
				}
			}
			case ACTION_DELETE:
			{
				if (IsValidEntity(entRef))
				{
					KillEntity(entRef);
					entRef = INVALID_ENT_REFERENCE;
					g_alBrushes.Set(entry.brushIndex, INVALID_ENT_REFERENCE);
					
					success = true;
				}
			}
		}
		
		if (!success)
		{
			// FUCKING PANIC
			g_iUndoIndex[client]--;
			PrintToChat(client, "WHAT THE FUCK WHAT HAPEHAPESHRASPEIJRHSAOLKEJR");
		}
	}
	else
	{
		PrintToChat(client, "Couldn't undo anything");
	}
	return Plugin_Handled;
}

public Action Command_SmDoublegrid(int client, int args)
{
	// this intentionally allows you to go above MAX_GRID_SIZE.
	if (g_fGridSize[client] < MAX_GRID_SIZE)
	{
		g_fGridSize[client] *= 2.0;
	}
	return Plugin_Handled;
}

public Action Command_SmHalvegrid(int client, int args)
{
	g_fGridSize[client] *= 0.5;
	return Plugin_Handled;
}

public Action Command_SmGridsize(int client, int args)
{
	if (args != 1)
	{
		PrintToChat(client, "Invalid number of arguments passed");
		return Plugin_Handled;
	}
	
	char szNumber[32];
	GetCmdArg(1, szNumber, sizeof szNumber);
	g_fGridSize[client] = StringToFloat(szNumber);
	
	return Plugin_Handled;
}
// TODO: save grid size and other settings in the file
public Action Command_SmSavemap(int client, int args)
{
	if (args != 1)
	{
		PrintToChat(client, "Invalid number of arguments passed");
		return Plugin_Handled;
	}
	
	char name[64];
	GetCmdArg(1, name, sizeof name);
	
	char path[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, path, sizeof path, SAVED_FILE_PATH);
	CreateDirectory(path, 511);
	Format(path, sizeof path, "%s%s%s", path, name, FILE_EXTENSION);
	
	if (FileExists(path))
	{
		// TODO: overwriting with confirmation?
		// PrintToChat(client, "File already exists! \"%s\"", path);
		// return Plugin_Handled;
	}
	
	File file = OpenFile(path, "wb");
	if (file != null)
	{
		// Header
		file.WriteString(FILE_IDENTIFIER, false);
		file.WriteInt32(FILE_MAGIC_NUMBER);
		file.WriteInt32(FILE_VERSION);
		
		file.WriteInt32(g_alBrushes.Length);
		for (int i = 0; i < g_alBrushes.Length; i++)
		{
			int entRef = g_alBrushes.Get(i);
			if (IsValidEntity(entRef))
			{
				CuboidBrush brush;
				GetEntPropVector(entRef, Prop_Send, "m_vecOrigin", brush.origin);
				GetEntPropVector(entRef, Prop_Send, "m_vecMaxs", brush.size);
				// NOTE: warning 229 tag index mismatch on brushes: this warning is ignored
				file.Write(brush, sizeof brush, 4);
			}
		}
		PrintToChat(client, "Saved map to \"%s\"", path);
		file.Close();
	}
	else
	{
		PrintToChat(client, "Failed to save file to: \"%s\"", path);
	}
	
	return Plugin_Handled;
}

public Action Command_SmLoadmap(int client, int args)
{
	if (args != 1)
	{
		PrintToChat(client, "Invalid number of arguments passed");
		return Plugin_Handled;
	}
	
	// TODO: this should be a menu
	
	char name[64];
	GetCmdArg(1, name, sizeof name);
	
	char path[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, path, sizeof path, SAVED_FILE_PATH);
	Format(path, sizeof path, "%s%s%s", path, name, FILE_EXTENSION);
	
	if (!FileExists(path))
	{
		// TODO: overwriting with confirmation?
		PrintToChat(client, "File doesn't exist! \"%s\"", path);
		return Plugin_Handled;
	}
	
	File file = OpenFile(path, "rb");
	if (file != null)
	{
		// Header
		char ident[5];
		file.ReadString(ident, sizeof ident);
		if (!StrEqual(ident, FILE_IDENTIFIER))
		{
			PrintToChat(client, "ERROR: Invalid identifier: \"%s\"", ident);
			file.Close();
			return Plugin_Handled;
		}
		int magicNumber;
		file.ReadInt32(magicNumber);
		
		if (magicNumber != FILE_MAGIC_NUMBER)
		{
			PrintToChat(client, "ERROR: Invalid magic number: \"%i\"", magicNumber);
			file.Close();
			return Plugin_Handled;
		}
		
		int fileVersion;
		file.ReadInt32(fileVersion);
		if (fileVersion > FILE_VERSION)
		{
			PrintToChat(client, "ERROR: File version too new: \"%i\", expected \"%i\"", fileVersion, FILE_VERSION);
			file.Close();
			return Plugin_Handled;
		}
		
		int brushCount;
		if (!file.ReadInt32(brushCount))
		{
			PrintToChat(client, "ERROR: File reading failed unexpectedly");
			file.Close();
			return Plugin_Handled;
		}
		
		CuboidBrush[] brushes = new CuboidBrush[brushCount];
		for (int i = 0; i < brushCount; i++)
		{
			// NOTE: warning 229 tag index mismatch on brushes: this warning is ignored
			if (file.Read(brushes[i], sizeof brushes[], 4) == -1)
			{
				PrintToChat(client, "ERROR: File reading failed unexpectedly");
				file.Close();
				return Plugin_Handled;
			}
		}
		
		// delete all the existing brushes and reset other stuff too 
		g_iActiveBrushIndex[client] = -1;
		g_alUndoState[client].Clear();
		for (int i = 0; i < g_alBrushes.Length; i++)
		{
			KillEntity(g_alBrushes.Get(i));
		}
		
		for (int i = 0; i < brushCount; i++)
		{
			int entRef = CreateCuboidProp(brushes[i].origin, brushes[i].size);
			if (IsValidEntity(entRef))
			{
				g_alBrushes.Push(entRef);
			}
			else
			{
				// FATAL FUCKING ERROR
				PrintToChatAll("FATAL FUCKING ERROR: Couldn't create brush entity when opening map file");
				SetFailState("FATAL FUCKING ERROR: Couldn't create brush entity when opening map file");
			}
		}
		PrintToChatAll("Loaded Map \"%s\"", name);
		file.Close();
	}
	else
	{
		PrintToChat(client, "Failed to open file: \"%s\"", path);
	}
	
	return Plugin_Handled;
}

public Action Command_SmExportvmf(int client, int args)
{
	if (args != 1)
	{
		PrintToChat(client, "Invalid number of arguments passed");
		return Plugin_Handled;
	}
	
	char name[64];
	GetCmdArg(1, name, sizeof name);
	
	char path[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, path, sizeof path, SAVED_FILE_PATH);
	CreateDirectory(path, 511);
	Format(path, sizeof path, "%s%s.vmf", path, name);
	
	if (FileExists(path))
	{
		// TODO: overwriting with confirmation?
		// PrintToChat(client, "File already exists! \"%s\"", path);
		// return Plugin_Handled;
	}
	
	File file = OpenFile(path, "w");
	if (file != null)
	{
		// Header
		file.WriteString(g_szVmfStart, false);
		
		// TODO: dynamic size based on brush count
		int solidId = 0;
		/*
		float uaxes = {{1, 0, 0},
			{0, 1, 0},
			{0, 0, 1}};
		
		
		float vaxes = {{-1, 0, 0},
			{0, -1, 0},
			{0, 0, -1}};
		*/
		for (int i = 0; i < g_alBrushes.Length; i++)
		{
			int entRef = g_alBrushes.Get(i);
			if (IsValidEntity(entRef))
			{
				CuboidBrush brush;
				GetEntPropVector(entRef, Prop_Send, "m_vecOrigin", brush.origin);
				GetEntPropVector(entRef, Prop_Send, "m_vecMaxs", brush.size);
				
				if (brush.size[0] <= 0.0
					|| brush.size[1] <= 0.0
					|| brush.size[2] <= 0.0)
				{
					continue;
				}
				// NOTE: warning 229 tag index mismatch on brushes: this warning is ignored
				
				// TODO: replace writelines with a single bulk write.
				solidId++;
				file.WriteLine("	solid");
				file.WriteLine("	{");
				file.WriteLine("		\"id\" \"%i\"", solidId);
				
				// sides
				for (int side = 0; side < SIDE_COUNT; side++)
				{
					int axis = side / 2;
					
					// start
					file.WriteLine("		side");
					file.WriteLine("		{");
					
					file.WriteLine("			\"id\" \"%i\"", side + 1);
					
					file.WriteLine("			\"plane\" \"(%f %f %f) (%f %f %f) (%f %f %f)\"",
						g_fPlanes[side][0][0] * brush.size[0] + brush.origin[0],
						g_fPlanes[side][0][1] * brush.size[1] + brush.origin[1],
						g_fPlanes[side][0][2] * brush.size[2] + brush.origin[2],
						g_fPlanes[side][1][0] * brush.size[0] + brush.origin[0],
						g_fPlanes[side][1][1] * brush.size[1] + brush.origin[1],
						g_fPlanes[side][1][2] * brush.size[2] + brush.origin[2],
						g_fPlanes[side][2][0] * brush.size[0] + brush.origin[0],
						g_fPlanes[side][2][1] * brush.size[1] + brush.origin[1],
						g_fPlanes[side][2][2] * brush.size[2] + brush.origin[2]
					);
					
					// TODO: top bottom sides materials separate
					if (side == SIDE_TOP)
					{
						file.WriteLine("			\"material\" \"%s\"", TOP_MATERIAL);
					}
					else if (side == SIDE_BOTTOM)
					{
						file.WriteLine("			\"material\" \"%s\"", BOTTOM_MATERIAL);
					}
					else
					{
						file.WriteLine("			\"material\" \"%s\"", SIDE_MATERIAL);
					}
					
					int uaxisIndex = ((axis * 2) + 2) % SIDE_COUNT;
					int vaxisIndex = ((axis * 2) + 5) % SIDE_COUNT;
					file.WriteLine("			\"uaxis\" \"[%f %f %f 0] 0.25\"", g_fNormals[uaxisIndex][0], g_fNormals[uaxisIndex][1], g_fNormals[uaxisIndex][2]);
					file.WriteLine("			\"vaxis\" \"[%f %f %f 0] 0.25\"", g_fNormals[vaxisIndex][0], g_fNormals[vaxisIndex][1], g_fNormals[vaxisIndex][2]);
					file.WriteLine("			\"rotation\" \"0\"");
					file.WriteLine("			\"lightmapscale\" \"16\"");
					file.WriteLine("			\"smoothing_groups\" \"0\"");
					
					// end
					file.WriteLine("		}");
				}
				// end
				file.WriteLine("	}");
			}
		}
		
		file.WriteString(g_szVmfEnd, false);
		PrintToChat(client, "Saved map to \"%s\"", path);
		file.Close();
	}
	else
	{
		PrintToChat(client, "Failed to save file to: \"%s\"", path);
	}
	
	return Plugin_Handled;
}